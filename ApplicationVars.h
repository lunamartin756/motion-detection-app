#ifndef APPLICATIONVARS_H
#define APPLICATIONVARS_H

#include <QObject>

class ApplicationVars : public QObject
{
    Q_OBJECT
public:
    ApplicationVars();
    Q_INVOKABLE void setRecordingPath(QString path);
    Q_INVOKABLE QString getRecordingPath();

private:
    QString m_recordingPath;

};

#endif // APPLICATIONVARS_H
