QT += quick
QT += multimedia
QT += multimedia-private

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        ApplicationVars.cpp \
        MotionFilter.cpp \
        QVideoFrameToQImage.cpp \
        VideoFrame.cpp \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    ApplicationVars.h \
    MotionFilter.h \
    QVideoFrameToQImage.h \
    VideoFrame.h



# Configuration to use opencv
# https://wiki.qt.io/How_to_setup_Qt_and_openCV_on_Windows

INCLUDEPATH += C:\opencv\build\include
LIBS  += "C:\opencv\release\bin"
LIBS += C:\opencv-build\bin\libopencv_core455.dll
LIBS += C:\opencv-build\bin\libopencv_highgui455.dll
LIBS += C:\opencv-build\bin\libopencv_imgcodecs455.dll
LIBS += C:\opencv-build\bin\libopencv_imgproc455.dll
LIBS += C:\opencv-build\bin\libopencv_features2d455.dll
LIBS += C:\opencv-build\bin\libopencv_videoio455.dll
