#include "MotionFilter.h"
#include "QVideoFrameToQImage.h"
#include "VideoFrame.h"
#include <iostream>
#include <opencv2/videoio.hpp>
#include<opencv2/imgcodecs.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include <QDateTime>

#include <QFile>

MotionFilter::MotionFilter(QObject *parent)
    : QAbstractVideoFilter( parent )
{

}

//--------------------------------------------------------------

QVideoFilterRunnable* MotionFilter::createFilterRunnable()
{
    return _video_filter;
};

//--------------------------------------------------------------

const QString &MotionFilter::savingPath() const
{
    return m_savingPath;
}

//--------------------------------------------------------------

void MotionFilter::setSavingPath(const QString &newSavingPath)
{
    if (m_savingPath == newSavingPath)
        return;
    m_savingPath = newSavingPath;
    _video_filter->setSavingDirectory(newSavingPath);
    emit savingPathChanged();
}
//--------------------------------------------------------------

bool MotionFilter::recordingStatus() const
{
    return m_recordingStatus;
}
//--------------------------------------------------------------

void MotionFilter::setRecordingStatus(bool newRecordingStatus)
{
    if (m_recordingStatus == newRecordingStatus)
        return;
    m_recordingStatus = newRecordingStatus;
    _video_filter->setRunnableRecordingStatus(newRecordingStatus);
    emit recordingStatusChanged();
}


//--------------------------------------------------------------
//=================== MotionfilterRunnable =======================

MotionFilterRunnable::MotionFilterRunnable()
{
}

//--------------------------------------------------------------
extern QImage qt_imageFromVideoFrame( const QVideoFrame &f);
QVideoFrame MotionFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    Q_UNUSED(surfaceFormat)
    Q_UNUSED(flags)


    if(!input){
        return QVideoFrame();
    }

    //Getting frame from QML
    QImage image = qt_imageFromVideoFrame(*input);

    //Converting qml image to opencv mat
    cv::Mat frame (image.height(), image.width(), CV_8UC4, (void *) image.constBits(), image.bytesPerLine());
    cv::flip(frame, frame, 0); //right orientation of the image
    //cv::Mat frame_clone = frame.clone();
    //cv::cvtColor(frame_clone,frame_clone, cv::COLOR_RGB2BGR); //Necessary to record video

    //=========================== Image processing  =====================================

    text = "No movement";
    cv::cvtColor(frame, grey, cv::COLOR_BGR2GRAY);
    cv::GaussianBlur(grey, gaussian, cv::Size(21, 21), 0, 0);
    cv::blur(gaussian, blur, cv::Size(5, 5));

    greyscale_image = blur.clone();

    //Setting first frame
    if (first_frame.empty()) {
        first_frame = greyscale_image.clone();
    }

    // Evaluating
    cv::absdiff(first_frame, greyscale_image, frame_delta);
    cv::threshold(frame_delta, thresh, 100, 255, cv::THRESH_BINARY);

    //std::cout<<cv::sum(frame_delta)[0]<<std::endl<<std::flush;

    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(1,1));
    cv::dilate(thresh, dilate_image, kernel);

    // Contours
    std::vector < std::vector < cv::Point >> contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::findContours(dilate_image, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

    std::vector<cv::Rect> boundRect(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        if (cv::contourArea(contours.at(i)) > 300) {
            boundRect[i] = cv::boundingRect(contours[i]);
            rectangle(frame, boundRect[i].tl(), boundRect[i].br(), cv::Scalar(0, 255, 0), 2);
            text = "Movement detected";
        }
    }

    // Text
    QString time_format = "yyyy-MM-dd  HH:mm:ss";
    QDateTime time = QDateTime::currentDateTime();
    QString timeString = time.toString(time_format);
    //std::cout<<timeString.toStdString()<<std::endl<<std::flush;

    cv::putText(frame, "[+] Status: " + text, cv::Point(10, frame.rows - 10), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 2);
    cv::putText(frame, timeString.toStdString(), cv::Point(frame.cols - 200, frame.rows - 10), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 2);

    // Recording
    if (runnable_status_recording == true){
        cv::Mat frame_clone = frame.clone();
        cv::cvtColor(frame_clone,frame_clone, cv::COLOR_RGB2BGR);

        vFrames.push_back(frame_clone);
        //std::cout<<"Filling vector frame"<<std::endl<<std::flush;
    }
    // Saving video
    if((runnable_status_recording == false) && (!vFrames.empty())){
        // Setting saving path for saving
        QString recordingTime = timeString;
        std::replace(recordingTime.begin(), recordingTime.end(), ' ', '_');
        std::replace(recordingTime.begin(), recordingTime.end(), ':', '-');

        video_path = m_recordingPath + "/surveillance"+ recordingTime+ ".avi";
        std::cout<<"Saving video: "<<video_path.toStdString()<<std::endl<<std::flush;

        int frame_width = vFrames.at(0).cols;
        int frame_height = vFrames.at(0).rows;
        int numberFrames = vFrames.size();
        std::cout<<"Frames: "<<numberFrames<<std::endl<<std::flush;
        cv::VideoWriter video(video_path.toStdString(), cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), 30, cv::Size(frame_width, frame_height));

        for(int i=0; i<numberFrames;i++){
            video.write(vFrames.at(i));
        }

        video.release();
        vFrames.clear();
    }

    // Returning opencv image to QImage
    cv::flip(frame, frame,0);
    //cv::imshow("Imagen prcesada",frame);
    QImage imageReturned;
    imageReturned = QImage (frame.data,
                            frame.cols,
                            frame.rows,
                            frame.step,
                            QImage::Format_RGB32).convertToFormat(QImage::Format_ARGB32);


    return imageReturned;



    //return *input;
}

