#ifndef MOTIONFILTER_H
#define MOTIONFILTER_H

#include <QObject>
#include <QAbstractVideoFilter>
#include <QVideoFilterRunnable>
#include "opencv2/opencv.hpp"
#include <vector>

//=================================================================================
//=================================================================================

class MotionFilterRunnable : public QObject, public QVideoFilterRunnable
{
    Q_OBJECT
private:
    QString m_recordingPath;
    std::vector<cv::Mat> vFrames;
    cv::Mat first_frame, grey, gaussian, blur, greyscale_image, frame_delta, thresh, dilate_image;
    std::string text;
    QString video_path;
    bool runnable_status_recording = false;

public:
    MotionFilterRunnable();
    QVideoFrame run( QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags ) Q_DECL_OVERRIDE;
    void setRunnableRecordingStatus(bool newRecording){runnable_status_recording = newRecording;};
    void setSavingDirectory(const QString &path){m_recordingPath = path;};
};




//=================================================================================
//=================================================================================

class MotionFilter : public QAbstractVideoFilter
{
    Q_OBJECT
    Q_PROPERTY(QString savingPath READ savingPath WRITE setSavingPath NOTIFY savingPathChanged)
    Q_PROPERTY(bool recordingStatus READ recordingStatus WRITE setRecordingStatus NOTIFY recordingStatusChanged)

public:
    MotionFilter(QObject *parent = nullptr);
    QVideoFilterRunnable* createFilterRunnable() Q_DECL_OVERRIDE;

    const QString &savingPath() const; // getter
    void setSavingPath(const QString &newSavingPath); //setter

    bool recordingStatus() const;//getter
    void setRecordingStatus(bool newRecordingStatus); //setter

signals:

    void savingPathChanged();
    void recordingStatusChanged();

private:
    QString m_savingPath;
    MotionFilterRunnable *_video_filter = new MotionFilterRunnable();
    bool m_recordingStatus;
};

#endif // MOTIONFILTER_H
