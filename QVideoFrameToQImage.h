// For more information about this class check:
// https://github.com/stephenquan/MyVideoFilterApp

#ifndef QVIDEOFRAMETOQIMAGE_H
#define QVIDEOFRAMETOQIMAGE_H


#include <QVideoFrame>
#include <QImage>

QImage QVideoFrameToQImage( const QVideoFrame& videoFrame );

#endif // QVIDEOFRAMETOQIMAGE_H
