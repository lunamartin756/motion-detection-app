# Aplicación de detección de movimiento
Aplicacción desarrolla para la adetección de movimiento implementado en C++ y QML (Qt 5.14.2), utilizando Opencv.  
Adiocalmente se agrega la opción de grabar el streaming que se está realizando. Este archivo se guarda en el directorio de videos de la computadora, en la carpeta "surveillanceRecordings" que sea crea automanicamente.


<h2>Demostración</h2>
![Motion_Detector_2023-05-31_22-09-00](/uploads/7d87c60e80dc05b8b2073c2fe58c7ffe/Motion_Detector_2023-05-31_22-09-00.mp4)
