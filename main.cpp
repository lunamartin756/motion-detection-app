#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <iostream>
#include <QDir>
#include "MotionFilter.h"
#include "ApplicationVars.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    //===========================================
    //======= Creating Recording Directory  =====
    //===========================================
    QStringList generalPaths = QStandardPaths::standardLocations(QStandardPaths::MoviesLocation);
    QString recordingPath = generalPaths.at(0) + "/surveillanceRecordings";
    std::cout<< "Recording path: "<<recordingPath.toStdString()<<std::endl<<std::flush;

    //Creating directory to save videos
    if(!QDir(recordingPath).exists()){
        QDir().mkdir(recordingPath);
    }

    ApplicationVars* appVars = new ApplicationVars();
    appVars->setRecordingPath(recordingPath);

    QGuiApplication app(argc, argv);

    qmlRegisterType<MotionFilter>("MotionFilter", 1, 0, "MotionFilter");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.rootContext()->setContextProperty("AppVars", appVars);

    engine.load(url);

    return app.exec();
}
