import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.5
import QtMultimedia 5.9
import MotionFilter 1.0

Window {
    id: root
    width: 1360
    height: 960
    visible: true
    title: qsTr("Motion Detector")
    color: "#376AB0"
    x: Screen.width / 2 - width / 2
    y: Screen.height / 2 - height / 2

    // =============================  Video  ============================
    Rectangle{
        id: videoContainer
        height: root.height
        width: root.width - 360
        color: "black"
        x: 360

        Camera{
            id: cameraId
            videoRecorder.frameRate: 30
        }
        VideoOutput{
            id: videoOutput
            source: cameraId
            anchors.fill: parent
            filters: [motionFilter]
        }
    }


    // ==============================  Menu  =============================
    Item{
        id: menuContainer
        height: root.height
        width: root.width - videoContainer.width
        x: 0

        Text{
            id: menuTitle
            x: 22
            y: 30
            color: "white"
            text: "Video Surveillance"
            font.pixelSize: 38
        }

        Text{
            id: menuOptions
            x: 54
            y: 200
            color: "white"
            text: "Video Options:"
            font.pixelSize: 38
        }

        Column{
            anchors.horizontalCenter: menuContainer.horizontalCenter
            anchors.top: menuOptions.bottom
            anchors.topMargin: 30
            spacing: 40

            RoundButton{
                id: startButton
                height: 60
                width: 160
                text: "Start"
                font.pixelSize: 24

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        startButton.scale = 1.2
                    }

                    onExited: {
                        startButton.scale = 1
                    }

                    onClicked: {
                        console.log("StartButton clicked")
                        stopButton.enabled = true
                        recordButton.enabled = true
                        cameraId.start()
                    }
                }


            }

            RoundButton{
                id: stopButton
                height: 60
                width: 160
                text: "Stop"
                font.pixelSize: 24
                enabled: false

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        stopButton.scale = 1.2
                    }

                    onExited: {
                        stopButton.scale = 1
                    }

                    onClicked: {
                        console.log("StopButton clicked")
                        stopButton.enabled = false
                        recordButton.enabled = false
                        stopRecordingButton.enabled = false
                        cameraId.stop()
                        motionFilter.recordingStatus = false
                    }
                }
            }
        }
        Text{
            id: recordingOptions
            x: 25
            y: 490
            color: "white"
            text: "Recording options:"
            font.pixelSize: 38
        }

        Row{
            anchors.horizontalCenter: menuContainer.horizontalCenter
            anchors.top: recordingOptions.bottom
            anchors.topMargin: 20
            spacing: 30

            RoundButton{
                id: recordButton
                height: 40
                width: 140
                text: "Record"
                font.pixelSize: 24
                enabled: false

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        recordButton.scale = 1.2
                    }

                    onExited: {
                        recordButton.scale = 1
                    }

                    onClicked: {
                        console.log("recordButton clicked")
                        stopRecordingButton.enabled = true
                        recordButton.enabled = false
                        motionFilter.recordingStatus = true
                        console.log("Recording status: " + motionFilter.recordingStatus)

                    }
                }
            }

            RoundButton{
                id: stopRecordingButton
                height: 40
                width: 140
                text: "Stop"
                font.pixelSize: 24
                enabled: false

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        stopRecordingButton.scale = 1.2
                    }

                    onExited: {
                        stopRecordingButton.scale = 1
                    }

                    onClicked: {
                        console.log("stopRecordingButton clicked")
                        recordButton.enabled = true
                        stopRecordingButton.enabled = false
                        motionFilter.recordingStatus = false
                        console.log("Recording status: " + motionFilter.recordingStatus)

                    }
                }
            }

        }


    }

    Text{
        id: filterText
        anchors.top: videoContainer.top
        anchors.topMargin: 20
        anchors.horizontalCenter: videoContainer.horizontalCenter
        color: "white"
        text: "Active Filter: Motion Detection"
        font.pixelSize: 38
    }

    MotionFilter{
        id: motionFilter
    }

    Component.onCompleted: {
        cameraId.stop()
        motionFilter.savingPath = AppVars.getRecordingPath()
    }

}
